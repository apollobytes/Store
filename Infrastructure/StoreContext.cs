﻿using Core.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Owned<Address>();
            modelBuilder.Entity<OrderItem>().HasOne<Product>();
            modelBuilder.Entity<Product>().HasData(
                new 
                {
                    Id = 1,
                    Name = "Jimmy Dean® Sausage, Egg & Cheese Croissant Sandwiches, 8 Count (Frozen)",
                    Description = "Rise and shine for a warm croissant sandwich. Savory pork sausage, fluffy eggs, and melty cheese all come together on a buttery croissant for a perfect start to your day. Packed with 12 grams of protein per serving, Jimmy Dean Sausage, Egg & Cheese Croissant Sandwiches give you more fuel to power your morning. Simply microwave and serve for a delicious breakfast at home or on-the-go. Includes 4 individually wrapped sandwiches. Can a breakfast sandwich make the world a brighter place? Thats a mighty big task for a sandwich, but at Jimmy Dean, were ready for it. When you start your morning off right it can lead to a great day, and you can even pass that sunshine on to others. And if everyone has a great day, imagine all the good we can do. So lets do it - lets shine it forward and make this world a brighter place one breakfast sandwich at a time.",
                    Price = 8.98m,
                    CategoryId = 4,
                    Url = "https://i5.walmartimages.com/asr/f0cfccff-3963-4434-81e3-e6d72045f15b_3.a5b0468addfa260d52b2c96aa1bd4def.png?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                },
                new 
                {
                    Id = 2,
                    Name = "Weight Watchers Smart Ones® Tasty American Favorites Mini Cheeseburger 6 ct Box",
                    Description = "Weight Watchers Smart Ones Mini Cheeseburgers are a tasty choice for people watching their diet. They're ideal for enjoying for lunch or dinner. These Weight Watchers mini cheeseburgers feature flame-broiled beef patties and cheese on mini-buns. They are 190 calories per serving and contain 9g of protein. Thes mini burgers are also ideal as an appetizer for parties.",
                    Price = 12.8m,
                    CategoryId = 4,
                    Url = "https://i5.walmartimages.com/asr/5f19add1-1fec-4913-b051-b49390d29e9f_1.77ef40591c789e0530ede39cea862e39.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                },
                new 
                {
                    Id = 3,
                    Name = "Great Value Organic Green Beans, 14.5oz",
                    Description = "Great Value Organic Cut Green Beans are picked and packed same day to deliver you with that fresh from the farm taste. Packed in water and lightly seasoned with sea salt, these canned green beans cook in minutes on the stove or in the microwave for your convenience. They are mild flavored beans that taste great on their own and can also be used as an ingredient in green bean recipes such as casseroles, salads and soups. Make it a healthy meal and serve them with grilled cauliflower and a fresh garden salad. Green beans are part of a healthy diet as they are a good source of nutrients. They are also great for health conscious individuals as they are USDA organic and gluten-free. Healthy dinner sides are made easy with Great Value Organic Cut Green Beans.",
                    Price = 1.48m,
                    CategoryId = 4,
                    Url = "https://i5.walmartimages.com/asr/fe6de678-26fe-4f71-8501-e643e2a1b53e_1.cf23c7f968c736b426f50ace446c6a31.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                },
                 new 
                 {
                     Id = 4,
                     Name = "Dole Pineapple Chunks in 100% Pineapple Juice 20 oz. Can",
                     Description = "Dole Pineapple Chunks in 100% Pineapple Juice are filled with all the deliciousness you find in whole pineapple, and there is no added sugar. Convenient, pre-cut chunks are great for snacking, at home or on-the-go, and can also be used in smoothies and cocktails.",
                     Price = 1.58m,
                    CategoryId = 4,
                     Url = "https://i5.walmartimages.com/asr/de4a2a05-154d-497c-b47e-561142876faf_2.a746fe89b1a5533f2386250a9edbd0e6.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                 },
                 new 
                 {
                     Id = 5,
                     Name = "Choice Extra Large Ribeye Steak, 2.75-3.25",
                     Description = "Whole Muscle Beef Beef Choice Angus Ribeye Steak Choice Extra Large Ribeye Steak, 2.75-3.25",
                     Price = 5.22m,
                     CategoryId = 2,
                     Url = "https://i5.walmartimages.com/asr/ed21a4a5-01bb-4906-979e-e4ac91aeb973_1.132eec47979647bff18fb9c705214ed5.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                 }, 
                 new 
                 {
                     Id = 6,
                     Name = "Angus Choice Beef Loin Bone-In T-Bone Steak, 1.0-2.0 lbs.",
                     Description = "Angus Choice Beef Loin Bone-In T-Bone Steak, 1.0-2.0 lbs. CHOICE T-BONE STEAK",
                     Price = 7.28m,
                     CategoryId = 2,
                     Url = "https://i5.walmartimages.com/asr/b07dd98f-09e6-488f-851b-47f83bc8b00d_1.441792e1b265741959122052f171e4ff.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                 }, 
                 new 
                 {
                     Id = 7,
                     Name = "Marketside Grass-Fed Beef Top Sirloin Steak, 8 oz",
                     Description = "Marketside Butcher Beef Top Sirloin Steak is made from premium cuts of grass-fed beef that is free to roam open pastures as nature intended. This lean steak is a smart choice for backyard barbecues with family and friends. This steak lends itself to a variety of preparations including baking, frying and grilling. Cut into strips, season with salt, pepper and rosemary, and grill for steak-house flavor. Serve prepared steak with steak sauce, a baked potato and green beans for a classically delicious meal. This cut is also an ideal cut for fajitas, stews, skewering with vegetables and more. This meat is USDA inspected and contains no added hormones or antibiotics. Experience the grass-fed difference with Marketside Butcher Beef Top Sirloin Steak.",
                     Price = 6.32m,
                     CategoryId = 2,
                     Url = "https://i5.walmartimages.com/asr/ce23d05d-e376-4376-bfc5-d8e38d6baf5b_1.c38426bede692d7350efb4b2bbb86a5f.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF"
                 }
                );
            modelBuilder.Entity<ProductCategory>().HasData(
                new ProductCategory { Id = 1, Name = "Organic Products" },
                new ProductCategory { Id = 2, Name = "Meat Products" },
                new ProductCategory { Id = 3, Name = "Fish and Seafood" },
                new ProductCategory { Id = 4, Name = "Frozen Products" },
                new ProductCategory { Id = 5, Name = "Sweets" }
                );
        }
    }
}
