﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 50, nullable: false),
                    Phone = table.Column<string>(maxLength: 50, nullable: true),
                    Address_Street = table.Column<string>(maxLength: 150, nullable: false),
                    Address_PostalCode = table.Column<string>(maxLength: 20, nullable: true),
                    Address_City = table.Column<string>(maxLength: 50, nullable: true),
                    Address_Country = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProductCategory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductCategory", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    Url = table.Column<string>(maxLength: 200, nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    CategoryId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_ProductCategory_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "ProductCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<int>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false),
                    OrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderItem_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderItem_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "ProductCategory",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Organic Products" },
                    { 2, "Meat Products" },
                    { 3, "Fish and Seafood" },
                    { 4, "Frozen Products" },
                    { 5, "Sweets" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "CategoryId", "Description", "Name", "Price", "Url" },
                values: new object[,]
                {
                    { 5, 2, "Whole Muscle Beef Beef Choice Angus Ribeye Steak Choice Extra Large Ribeye Steak, 2.75-3.25", "Choice Extra Large Ribeye Steak, 2.75-3.25", 5.22m, "https://i5.walmartimages.com/asr/ed21a4a5-01bb-4906-979e-e4ac91aeb973_1.132eec47979647bff18fb9c705214ed5.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 6, 2, "Angus Choice Beef Loin Bone-In T-Bone Steak, 1.0-2.0 lbs. CHOICE T-BONE STEAK", "Angus Choice Beef Loin Bone-In T-Bone Steak, 1.0-2.0 lbs.", 7.28m, "https://i5.walmartimages.com/asr/b07dd98f-09e6-488f-851b-47f83bc8b00d_1.441792e1b265741959122052f171e4ff.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 7, 2, "Marketside Butcher Beef Top Sirloin Steak is made from premium cuts of grass-fed beef that is free to roam open pastures as nature intended. This lean steak is a smart choice for backyard barbecues with family and friends. This steak lends itself to a variety of preparations including baking, frying and grilling. Cut into strips, season with salt, pepper and rosemary, and grill for steak-house flavor. Serve prepared steak with steak sauce, a baked potato and green beans for a classically delicious meal. This cut is also an ideal cut for fajitas, stews, skewering with vegetables and more. This meat is USDA inspected and contains no added hormones or antibiotics. Experience the grass-fed difference with Marketside Butcher Beef Top Sirloin Steak.", "Marketside Grass-Fed Beef Top Sirloin Steak, 8 oz", 6.32m, "https://i5.walmartimages.com/asr/ce23d05d-e376-4376-bfc5-d8e38d6baf5b_1.c38426bede692d7350efb4b2bbb86a5f.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 1, 4, "Rise and shine for a warm croissant sandwich. Savory pork sausage, fluffy eggs, and melty cheese all come together on a buttery croissant for a perfect start to your day. Packed with 12 grams of protein per serving, Jimmy Dean Sausage, Egg & Cheese Croissant Sandwiches give you more fuel to power your morning. Simply microwave and serve for a delicious breakfast at home or on-the-go. Includes 4 individually wrapped sandwiches. Can a breakfast sandwich make the world a brighter place? Thats a mighty big task for a sandwich, but at Jimmy Dean, were ready for it. When you start your morning off right it can lead to a great day, and you can even pass that sunshine on to others. And if everyone has a great day, imagine all the good we can do. So lets do it - lets shine it forward and make this world a brighter place one breakfast sandwich at a time.", "Jimmy Dean® Sausage, Egg & Cheese Croissant Sandwiches, 8 Count (Frozen)", 8.98m, "https://i5.walmartimages.com/asr/f0cfccff-3963-4434-81e3-e6d72045f15b_3.a5b0468addfa260d52b2c96aa1bd4def.png?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 2, 4, "Weight Watchers Smart Ones Mini Cheeseburgers are a tasty choice for people watching their diet. They're ideal for enjoying for lunch or dinner. These Weight Watchers mini cheeseburgers feature flame-broiled beef patties and cheese on mini-buns. They are 190 calories per serving and contain 9g of protein. Thes mini burgers are also ideal as an appetizer for parties.", "Weight Watchers Smart Ones® Tasty American Favorites Mini Cheeseburger 6 ct Box", 12.8m, "https://i5.walmartimages.com/asr/5f19add1-1fec-4913-b051-b49390d29e9f_1.77ef40591c789e0530ede39cea862e39.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 3, 4, "Great Value Organic Cut Green Beans are picked and packed same day to deliver you with that fresh from the farm taste. Packed in water and lightly seasoned with sea salt, these canned green beans cook in minutes on the stove or in the microwave for your convenience. They are mild flavored beans that taste great on their own and can also be used as an ingredient in green bean recipes such as casseroles, salads and soups. Make it a healthy meal and serve them with grilled cauliflower and a fresh garden salad. Green beans are part of a healthy diet as they are a good source of nutrients. They are also great for health conscious individuals as they are USDA organic and gluten-free. Healthy dinner sides are made easy with Great Value Organic Cut Green Beans.", "Great Value Organic Green Beans, 14.5oz", 1.48m, "https://i5.walmartimages.com/asr/fe6de678-26fe-4f71-8501-e643e2a1b53e_1.cf23c7f968c736b426f50ace446c6a31.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" },
                    { 4, 4, "Dole Pineapple Chunks in 100% Pineapple Juice are filled with all the deliciousness you find in whole pineapple, and there is no added sugar. Convenient, pre-cut chunks are great for snacking, at home or on-the-go, and can also be used in smoothies and cocktails.", "Dole Pineapple Chunks in 100% Pineapple Juice 20 oz. Can", 1.58m, "https://i5.walmartimages.com/asr/de4a2a05-154d-497c-b47e-561142876faf_2.a746fe89b1a5533f2386250a9edbd0e6.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_OrderId",
                table: "OrderItem",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_ProductId",
                table: "OrderItem",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_CustomerId",
                table: "Orders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "OrderItem");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "ProductCategory");
        }
    }
}
