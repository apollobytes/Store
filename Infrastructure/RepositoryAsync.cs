﻿using Core.Interfaces;
using Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class RepositoryAsync<T> : IRepositoryAsync<T> where T : BaseEntity
    {
        StoreContext _context;

        public RepositoryAsync(StoreContext context)
        {
            _context = context;
        }

        public async Task<T> AddAsync(T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(T entity)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public Task<T> GetByIdAsync(int id)
        {
            return _context.Set<T>().FindAsync(id);
        }

        public Task<T> GetOne(ISpecification<T> specification)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<T>> ListAllAsync()
        {
            return _context.Set<T>().ToListAsync();
        }

        public Task<List<T>> ListAsync(ISpecification<T> spec)
        {
            var query = spec.Includes.Aggregate(_context.Set<T>().AsQueryable(), (current, include) => current.Include(include));
            query = spec.IncludeStrings.Aggregate(query, (current, includeString) => current.Include(includeString));
            query = query.Where(spec.Criteria);

            return query.ToListAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _context.Update(entity);
            await _context.SaveChangesAsync();
        }
    }
}
