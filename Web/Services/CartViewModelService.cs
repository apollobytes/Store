﻿using Web.Interfaces;
using Web.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Web.Services
{
    public class CartViewModelService : ICartViewModelService
    {
        private const string IdentifierKey = "IdentifierKey";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDictionary<Guid, CartViewModel> _carts = new ConcurrentDictionary<Guid, CartViewModel>();
        ISession _session => _httpContextAccessor.HttpContext.Session;

        public async Task<int> CountAsync()
        {
            return (await GetCartAsync()).Count;
        }

        public CartViewModelService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task AddToCartAsync(ProductViewModel item)
        {
            var cart = await GetCartAsync();
            cart.AddItem(item);
        }

        public async Task RemoveFromCartAsync(int id)
        {
            var cart = await GetCartAsync();
            cart.DeleteItem(id);
        }

        public async Task EditCartAsync(Dictionary<string,int> ids)
        {
            var cart = await GetCartAsync();
            cart.EditQuantities(ids);
        }

        public async Task<CartViewModel> GetCartAsync()
        {
            var identifier = await GetOrCreateIdentifierAsync();
            if (!_carts.TryGetValue(identifier, out var cart))
            {
                cart = new CartViewModel();
                _carts[identifier] = cart;
            }
            return cart;
        }

        private async Task<Guid> GetOrCreateIdentifierAsync()
        {
            await _session.LoadAsync();
            var str = _session.GetString(IdentifierKey);

            if (str == null || !Guid.TryParse(str, out var identifier))
            {
                identifier = Guid.NewGuid();
                _session.SetString(IdentifierKey, identifier.ToString());
            }

            return identifier;
        }

    }
}
