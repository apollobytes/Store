﻿using Web.Interfaces;
using Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using AutoMapper;
using Core.Specifications;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Web.Services
{
    public class ProductViewModelsService : IProductViewModelsService
    {
        private readonly IRepositoryAsync<Product> _productsRepository;
        private readonly IRepositoryAsync<ProductCategory> _categoryRepository;
        private readonly IMapper _mapper;

        public ProductViewModelsService(IRepositoryAsync<Product> productsRepository, IRepositoryAsync<ProductCategory> categoryRepository, IMapper mapper)
        {
            _productsRepository = productsRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<ProductCategoryViewModel> GetCategoriesAsync()
        {
            var categories = await _categoryRepository.ListAllAsync();
            var categoryModel = new ProductCategoryViewModel { Categories = categories.Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name }) };
            return categoryModel;
        }

        public async Task<IEnumerable<ProductViewModel>> GetProductsByCategory(int categoryId)
        {
            var products = await _productsRepository.ListAsync(new ProductsByCategoryIdSpecification(categoryId));
            return _mapper.Map<IEnumerable<ProductViewModel>>(products);
        }

        public async Task<ProductViewModel> GetProductByIdAsync(int id)
        {
            var product = await _productsRepository.GetByIdAsync(id);
            return _mapper.Map<ProductViewModel>(product);
        }

        public async Task<IEnumerable<ProductViewModel>> GetProductsAsync()
        {
            var products = await _productsRepository.ListAllAsync();
            return _mapper.Map<IEnumerable<ProductViewModel>>(products);
        }

    }
}
