﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Core.Interfaces;
using Core.Models;
using Web.Interfaces;
using Web.ViewModels;

namespace Web.Services
{
    public class OrderViewModelService : IOrderViewModelService
    {
        private readonly IOrderService _orderService;
        private readonly ICartViewModelService _cartService;
        private readonly IMapper _mapper;

        public OrderViewModelService(IOrderService orderService, ICartViewModelService cartService, IMapper mapper)
        {
            _orderService = orderService;
            _cartService = cartService;
            _mapper = mapper;
        }

        public async Task CheckoutAsync(OrderViewModel order)
        {
            var items = (await _cartService.GetCartAsync()).Items;

            order.Items = _mapper.Map<IEnumerable<OrderItemViewModel>>(items);
            var o = _mapper.Map<Order>(order);

            await _orderService.CheckoutAsync(o);
        }
    }
}
