﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Core.Models;
using Core.Interfaces;
using Web.ViewModels;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Web.Interfaces;

namespace Web.ViewComponents
{
    public class CategoriesViewComponent : ViewComponent
    {
        private readonly IProductViewModelsService _productsService;

        public CategoriesViewComponent(IProductViewModelsService productsService)
        {
            _productsService = productsService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categoryModel = await _productsService.GetCategoriesAsync();
            return View(categoryModel);
        }
    }
}
