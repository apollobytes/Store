﻿using Web.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewComponents
{
    public class CartViewComponent : ViewComponent
    {
        const string SessionProductIdsKey = "SessionProductIdsKey";
        private readonly ICartViewModelService _cartService;

        public CartViewComponent(ICartViewModelService cartService)
        {
            _cartService = cartService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            /*
            var str = HttpContext.Session.GetString(SessionProductIdsKey);
            var cartCount = 0;
            if (!string.IsNullOrEmpty(str))
            {
                var ids = JsonConvert.DeserializeObject<Dictionary<int,int>>(str);
                cartCount = ids.Count;
            }
            return View(cartCount);
            */
            return View(await _cartService.CountAsync());
        }
    }
}
