﻿using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Interfaces
{
    public interface IOrderViewModelService
    {
        Task CheckoutAsync(OrderViewModel order);
    }
}