﻿using Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Interfaces
{
    public interface ICartViewModelService
    {
        Task<CartViewModel> GetCartAsync();
        Task<int> CountAsync();
        Task AddToCartAsync(ProductViewModel item);
        Task RemoveFromCartAsync(int id);
        Task EditCartAsync(Dictionary<string, int> ids);
    }
}
