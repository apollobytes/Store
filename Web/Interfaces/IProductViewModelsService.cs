﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Web.ViewModels;

namespace Web.Interfaces
{
    public interface IProductViewModelsService
    {
        Task<ProductCategoryViewModel> GetCategoriesAsync();
        Task<ProductViewModel> GetProductByIdAsync(int id);
        Task<IEnumerable<ProductViewModel>> GetProductsAsync();
        Task<IEnumerable<ProductViewModel>> GetProductsByCategory(int categoryId);
    }
}