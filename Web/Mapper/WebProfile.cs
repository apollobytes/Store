﻿using AutoMapper;
using Core.Models;
using Web.ViewModels;

namespace Web.Mapper
{
    public class WebProfile : Profile
    {
        public WebProfile()
        {
            CreateMap<Product, ProductViewModel>().ReverseMap();
            CreateMap<Order, OrderViewModel>().ReverseMap();
            CreateMap<CartItemViewModel, OrderItemViewModel>();
            CreateMap<OrderItem, OrderItemViewModel>().ReverseMap();
            CreateMap<Address, AddressViewModel>();
        }
    }
}
