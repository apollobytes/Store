﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.ViewModels;
using AutoMapper;
using Web.Interfaces;
using Web.Filters;

namespace Web.Controllers
{
    [ValidateModelState]
    public class OrderController : Controller
    {
        private readonly IOrderViewModelService _orderService;
        private readonly ICartViewModelService _cartService;
        private readonly IMapper _mapper;

        public OrderController(IOrderViewModelService orderService, ICartViewModelService cartService, IMapper mapper)
        {
            _orderService = orderService;
            _cartService = cartService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Submit(OrderViewModel order)
        {
            await _orderService.CheckoutAsync(order);
            
            return View(order);
        }
    }
}