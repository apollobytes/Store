﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Interfaces;
using Core.Models;
using Web.Interfaces;
using Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Web.Filters;

namespace Web.Controllers
{
    [ValidateModelState]
    public class ProductsController : Controller
    {
        private readonly IProductViewModelsService _productsService;

        public ProductsController(IProductViewModelsService productsService)
        {
            _productsService = productsService;
        }
        public async Task<IActionResult> Index(ProductCategoryViewModel productCategory)
        {
            var products = productCategory.Category == 0 ?
                await _productsService.GetProductsAsync() :
                await _productsService.GetProductsByCategory(productCategory.Category);
            return View(products);
        }

        public async Task<IActionResult> Details(int id)
        {
            var product = await _productsService.GetProductByIdAsync(id);
            return View(product);
        }
    }
}