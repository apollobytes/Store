﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Interfaces;
using System.Collections.Generic;
using Web.Filters;

namespace Web.Controllers
{
    [ValidateModelState]
    public class CartController : Controller
    {
        const string SessionProductIdsKey = "SessionProductIdsKey";
        private readonly IProductViewModelsService _productsService;
        private readonly ICartViewModelService _cartService;

        public CartController(IProductViewModelsService productsService, ICartViewModelService cartService)
        {
            _productsService = productsService;
            _cartService = cartService;
        }

        public async Task<IActionResult> Index()
        {
            var cart = await _cartService.GetCartAsync();
            return View(cart);
        }

        public async Task<IActionResult> Add(int id)
        {
            var product = await _productsService.GetProductByIdAsync(id);
            await _cartService.AddToCartAsync(product);
            
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(Dictionary<string, int> items)
        {
            await _cartService.EditCartAsync(items);
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Delete(int id)
        {
            await _cartService.RemoveFromCartAsync(id);
            return RedirectToAction(nameof(Index));
        }
    }

}