﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Web.Extensions
{
    static class SessionExtensions
    {
        public static T GetObject<T>(this ISession session, string key) where T : class
        {
            var str = session.GetString(key);
            return string.IsNullOrEmpty(str) ? null : JsonConvert.DeserializeObject<T>(str);
        }

        public static void SetObject<T>(this ISession session, string key, T obj) where T : class
        {
            var str = JsonConvert.SerializeObject(obj);
            session.SetString(key, str);
        }
    }
}