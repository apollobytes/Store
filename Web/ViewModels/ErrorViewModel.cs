﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class ErrorViewModel
    {
        public string RequestId { get; internal set; }
        public bool ShowRequestId { get; set; }
    }
}
