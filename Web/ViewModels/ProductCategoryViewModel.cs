﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Web.ViewModels
{
    public class ProductCategoryViewModel
    {
        public int Category { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
    }
}
