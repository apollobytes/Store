﻿using System.Collections.Generic;
using System.Linq;

namespace Web.ViewModels
{
    public class OrderViewModel
    {
        public CustomerViewModel Customer { get; set; }
        public IEnumerable<OrderItemViewModel> Items { get; set; } = new List<OrderItemViewModel>();
        public decimal Total => Items.Sum(x => x.Total);
    }
}
