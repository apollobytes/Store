﻿using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels
{
    public class CartItemViewModel
    {
        public CartItemViewModel(ProductViewModel product, int quantity = 1)
        {
            ProductId = product.Id;
            UnitPrice = product.Price;
            ProductName = product.Name;
            Quantity = quantity;
        }

        public int ProductId { get; }
        public string ProductName { get; set; }
        public decimal UnitPrice { get; }
        public int Quantity { get; set; }
        public decimal Total => Quantity * UnitPrice;
    }
}