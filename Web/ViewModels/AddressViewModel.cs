﻿using System.ComponentModel.DataAnnotations;

namespace Web.ViewModels
{
    public class AddressViewModel
    {
        [Required]
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
