﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.ViewModels
{
    public class CartViewModel
    {
        private List<CartItemViewModel> _items = new List<CartItemViewModel>();
        public IReadOnlyCollection<CartItemViewModel> Items => _items.AsReadOnly();

        public void AddItem(ProductViewModel product, int quantity = 1)
        {
            var existingItem = _items.FirstOrDefault(x => x.ProductId == product.Id);
            if (existingItem != null)
            {
                existingItem.Quantity += quantity;
            }
            else
            {
                _items.Add(new CartItemViewModel(product, quantity));
            }
        }

        public void RemoveItem(int id, int quantity = 1)
        {
            var existingProduct = _items.FirstOrDefault(x => x.ProductId == id);
            if (existingProduct != null)
            {
                if (existingProduct.Quantity - quantity > 0)
                {
                    existingProduct.Quantity -= quantity;
                }
                else
                {
                    _items.Remove(existingProduct);
                }
            }
        }

        public void DeleteItem(int id)
        {
            var existingProduct = _items.FirstOrDefault(x => x.ProductId == id);
            if (existingProduct != null)
            {
                _items.Remove(existingProduct);
            }
        }

        public void EditQuantities(Dictionary<string, int> ids)
        {
            foreach (var p in ids)
            {
                var item = _items.FirstOrDefault(x => x.ProductId == int.Parse(p.Key));
                if (p.Value <= 0)
                {
                    _items.Remove(item);
                }
                item.Quantity = p.Value;
            }
        }

        public int Count => _items.Sum(x => x.Quantity);
        public decimal Total => _items.Sum(x => x.Total);
    }
}
