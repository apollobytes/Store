Store web application.

Technologies used:

ASP.NET Core MVC
EntityFramework Core
xUnit

Running the sample

After cloning or downloading the code you should run Update-Database from Package Manager Console, it will create a database using an MS SQL LocalDb using migrations and will populate the data.
Then the application can run.

Todos
- Authentication
- Custom Logging
- Specific Exceptions