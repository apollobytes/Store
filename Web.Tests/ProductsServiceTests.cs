﻿using AutoMapper;
using Core.Interfaces;
using Core.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Web.Interfaces;
using Web.Services;
using Web.ViewModels;
using Xunit;

namespace Web.Tests
{
    public class ProductsServiceTests
    {
        private readonly Mock<IRepositoryAsync<Product>> _fakeProductsRepository;
        private Mock<IMapper> _fakeMapper;

        public ProductsServiceTests()
        {
            _fakeProductsRepository = new Mock<IRepositoryAsync<Product>>();
            _fakeMapper = new Mock<IMapper>();
        }

        [Fact]
        public async Task GetProductsAsync_WhenCalled_ShouldReturnAllProducts()
        {
            //arrange
            _fakeProductsRepository.Setup(s => s.ListAllAsync()).Returns(Task.FromResult(GetProducts()));
            _fakeMapper.Setup(m => m.Map<IEnumerable<ProductViewModel>>(It.IsAny<IEnumerable<Product>>())).Returns(GetProductViewModels());
            var sut = new ProductViewModelsService(_fakeProductsRepository.Object, null, _fakeMapper.Object);

            //act
            var actual = await sut.GetProductsAsync();

            //assert
            var data = Assert.IsType<List<ProductViewModel>>(actual);
            Assert.Equal(3, data.Count);

        }

        private List<Product> GetProducts()
        {
            return new List<Product>
            {
                new Product
                {
                    Id = 1
                },
                new Product
                {
                    Id = 2
                },
                new Product
                {
                    Id = 3
                }
            };
        }

        private IEnumerable<ProductViewModel> GetProductViewModels()
        {
            return new List<ProductViewModel>
            {
                new ProductViewModel
                {
                    Id = 1,
                    CategoryId = 1,
                },
                new ProductViewModel
                {
                    Id = 2,
                    CategoryId = 4,
                },
                new ProductViewModel
                {
                    Id = 3,
                    CategoryId = 1
                }
            };
        }
    }
}
