﻿using System.Collections.Generic;
using Xunit;
using Moq;
using Web.Interfaces;
using Web.ViewModels;
using System.Threading.Tasks;
using Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Web.Tests
{
    public class ProductsControllerTest
    {
        private readonly Mock<IProductViewModelsService> _fakeProductService;

        public ProductsControllerTest()
        {
            _fakeProductService = new Mock<IProductViewModelsService>();
        }

        [Fact]
        public async Task Index_PassedCategory0_ReturnsAllProducts()
        {
            //arrange
            IEnumerable<ProductViewModel> result = GetProducts();
            _fakeProductService.Setup(s => s.GetProductsAsync()).Returns(Task.FromResult(result));

            var sut = new ProductsController(_fakeProductService.Object);

            //act
            var actual = await sut.Index(new ProductCategoryViewModel { Category = 0 });

            //assert
            var view = Assert.IsType<ViewResult>(actual);
            var data = Assert.IsType<List<ProductViewModel>>(view.Model);
            Assert.Equal(3, data.Count);
        }

        [Fact]
        public async Task Index_PassedCategory1_ReturnsCategory1Products()
        {
            //arrange
            IEnumerable<ProductViewModel> result = GetProducts();
            _fakeProductService.Setup(s => s.GetProductsByCategory(1)).Returns(Task.FromResult(result.Where(a => a.CategoryId == 1).AsEnumerable()));

            var sut = new ProductsController(_fakeProductService.Object);

            //act
            var actual = await sut.Index(new ProductCategoryViewModel { Category = 1 });

            //assert
            var view = Assert.IsType<ViewResult>(actual);
            var data = view.Model;
            Assert.Equal(2, ((IEnumerable<ProductViewModel>)data).Count());
        }

        private IEnumerable<ProductViewModel> GetProducts()
        {
            return new List<ProductViewModel>
            {
                new ProductViewModel
                {
                    Id = 1,
                    CategoryId = 1,
                },
                new ProductViewModel
                {
                    Id = 2,
                    CategoryId = 4,
                },
                new ProductViewModel
                {
                    Id = 3,
                    CategoryId = 1
                }
            };
        }

        [Fact]
        public async Task Details_WhenPassedId_ShouldReturnProduct()
        {
            //arrange
            var product = new ProductViewModel { Id = 1 };
            _fakeProductService.Setup(s => s.GetProductByIdAsync(1)).Returns(Task.FromResult(product));
            var sut = new ProductsController(_fakeProductService.Object);

            //act
            var actual = await sut.Details(1);

            //assert
            var view = Assert.IsType<ViewResult>(actual);
            var data = Assert.IsType<ProductViewModel>(view.Model);
            Assert.Equal(1, data.Id);
        }

        [Fact]
        public async Task Details_WhenPassedNotExistingId_ShouldReturnNull()
        {
            //arrange
            _fakeProductService.Setup(s => s.GetProductByIdAsync(0)).Returns(Task.FromResult((ProductViewModel)null));
            var sut = new ProductsController(_fakeProductService.Object);

            //act
            var actual = await sut.Details(0);

            //assert
            var view = Assert.IsType<ViewResult>(actual);
            var data = view.Model;
            Assert.Null(data);
        }
    }
}
