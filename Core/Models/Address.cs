﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class Address
    {
        [Required]
        [MaxLength(150)]
        public string Street { get; set; }
        [MaxLength(20)]
        public string PostalCode { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
    }
}
