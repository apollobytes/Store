﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models
{
    public class ProductCategory : BaseEntity
    {
        public string Name { get; set; }
    }
}
