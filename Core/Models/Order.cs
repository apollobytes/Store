﻿using System.Collections.Generic;
using System.Linq;

namespace Core.Models
{
    public class Order : BaseEntity
    {
        public IReadOnlyCollection<OrderItem> Items { get; set; }
        public Customer Customer { get; set; }
        public decimal Total => Items.Sum(x => x.Total);
    }
}
