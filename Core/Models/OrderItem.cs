﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class OrderItem : BaseEntity
    {
        public int ProductId { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        public decimal Total => Quantity * UnitPrice;
    }
}
