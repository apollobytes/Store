﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class Customer : BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(50)]
        public string Email { get; set; }
        [MaxLength(50)]
        public string Phone { get; set; }
        public Address Address { get; set; }
    }
}
