﻿using System.ComponentModel.DataAnnotations;

namespace Core.Models
{
    public class Product : BaseEntity
    {
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        [MaxLength(200)]
        public string Url { get; set; }
        [Required]
        public decimal Price { get; set; }
        public ProductCategory Category { get; set; }
    }
}
