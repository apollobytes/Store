﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IRepositoryAsync<Order> _orderRepository;

        public OrderService(IRepositoryAsync<Order> repository)
        {
            _orderRepository = repository;
        }

        public async Task CheckoutAsync(Order order)
        {
            await _orderRepository.AddAsync(order);
        }
    }
}
