﻿using Core.Models;

namespace Core.Specifications
{
    public class ProductsByCategoryIdSpecification : BaseSpecification<Product>
    {
        public ProductsByCategoryIdSpecification(int categoryId) : base(p => p.Category.Id == categoryId) { }
    }
}
