﻿using Core.Models;
using System.Threading.Tasks;

namespace Core.Interfaces
{
    public interface IOrderService
    {
        Task CheckoutAsync(Order order);
    }
}
