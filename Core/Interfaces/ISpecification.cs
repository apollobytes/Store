﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Core.Interfaces
{
    public interface ISpecification<T> where T : BaseEntity
    {
        Expression<Func<T,bool>> Criteria { get; }
        List<string> IncludeStrings { get; }
        List<Expression<Func<T, object>>> Includes { get; }
    }
}